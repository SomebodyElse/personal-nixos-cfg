{
  description = "System Config";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.05";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    
    home-manager = {
      url = "github:nix-community/home-manager/release-21.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };

#    flake-utils.url = "github:numtide/flake-utils";
    nur.url = "github:nix-community/NUR";
    emacs-overlay.url = "github:nix-community/emacs-overlay";
  };

  outputs = { nixpkgs, nixpkgs-unstable, home-manager, emacs-overlay, ... }: {
    nixosConfigurations = {
      framework = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./nixos/shared-config.nix
          ./nixos/framework-laptop.nix
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.nick = import ./home-manager/framework-laptop.nix;
          }
#          ({ pkgs, ...}: {
#            nixpkgs.overlays = [
#              emacs-overlay.overlay
#              (self: super: {
#                unstable = nixpkgs-unstable.legacyPackages.x86_64-linux;
#              })
#            ];
#          })
        ];
      };
    };
  };
}
