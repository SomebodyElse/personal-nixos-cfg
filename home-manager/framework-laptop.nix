{ pkgs, ... }:

{
  home.username = "nick";
  home.homeDirectory = "/home/nick";
  home.stateVersion = "21.05";
  
  imports = [ ./common.nix ./i3.nix ./polybar.nix ];
  xsession.scriptPath = ".hm-xsession";
  xdg = {
    enable = true;

    configFile = {
      "emacs.d" = {
        source = ../emacs.d;
        recursive = true;
      };


      "nixpkgs/config.nix".text = "{ allowUnfree = true; }";

    };
  };
}
