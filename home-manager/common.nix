{ pkgs, ... }:

{
  home.packages = with pkgs; [
    # GUI applications
    firefox
    brave
    discord
    pavucontrol
    xfce.thunar
    rpi-imager
    prusa-slicer
    kitty
    discord
    vlc
    signal-desktop
    psensor
    keepass
    alacritty
    compton
    

    # GUI utilities
    flameshot
    rofi
    i3status-rust
    feh
    copyq
    scrcpy
    peek

    # Command line utilities
    coreutils
    ripgrep
    ripgrep-all
    fd
    fzf
    bat
    tree
    jq
    trash-cli
    tealdeer
    ffmpeg
    imagemagick
    pandoc
    haskellPackages.FractalArt
    zip
    unzip
    p7zip
    gitAndTools.gh
    git-lfs
    ispell
    graphviz
    appimage-run
    xclip
    xdotool
    zsh
    xorg.xwininfo
    units
    inotify-tools
    cachix
    arandr


    # Command line fun
    fortune
    cowsay

    # Compilers/programming language stuff
    clang
    gnumake
    libtool
    cmake
    gradle
    openjdk
    python38
    nixfmt
    zig
    nodejs
    leiningen

    # IDEs
    jetbrains.idea-community
    emacs27

    # Fonts
    fira
    fira-mono
    montserrat
    overpass
    noto-fonts
    noto-fonts-cjk
    noto-fonts-extra
    noto-fonts-emoji
    noto-fonts-emoji-blob-bin
  ];

#  programs.emacs = {
#    enable = true;
#    package = pkgs.emacsPgtkGcc;
#    extraPackages = (epkgs: [ epkgs.vterm ]);
#  };
#  services.emacs.enable = true;
#  services.emacs.client.enable = true;

  programs.direnv = {
    enable = true;
#    enableFishIntegration = true;
    nix-direnv.enable = true;
  };

  services.dunst.enable = true;
  services.dunst.settings = {
    global = {
      transparency = 10;
      geometry = "700-30+20";
    };
  };

  fonts.fontconfig.enable = true;

}
