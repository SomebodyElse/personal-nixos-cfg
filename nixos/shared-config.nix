{ config, pkgs, ... }:

{
  boot.kernel.sysctl = { "kernel.sysrq" = 1; };

  nix = {
    autoOptimiseStore = true;
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  nixpkgs.config.allowUnfree = true;

  networking.networkmanager.enable = true;
  networking.useDHCP = false;

  # Enable i3wm services.xserver.enable = true;
  services.xserver.enable = true;
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.libinput.enable = true;
  services.xserver.desktopManager.xterm.enable = true;
  services.xserver.desktopManager.session = [
    {
      name = "home-manager";
      start = ''
            ${pkgs.runtimeShell} $HOME/.hm-xsession &
            waitPID=$!
            '';
    }
  ];
  #services.xserver.windowManager.i3.enable = true;

  # Configure keymap in X11
  services.xserver.layout = "us";
  
  # Disable mouse acceleration
  services.xserver.libinput.mouse.accelProfile = "flat";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull;
  hardware.pulseaudio.enable = true;

  fonts = {
    fontDir.enable = true;
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      inconsolata
      fira-mono
      ubuntu_font_family
    ];
  };
  
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.nick = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkManager" "video" ]; # Enable ‘sudo’ for the user.
        
  };

  # System-level packages
  environment.systemPackages = with pkgs; [
    git
    wget
    vim
    gcc
    rdiff-backup # for backup
    dconf
    powertop
  ];

  services.interception-tools.enable = true;
  services.picom.enable = true;
  services.earlyoom.enable = true;

  system.stateVersion = "21.05";
}
