{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  # boot settings
  boot.initrd.availableKernelModules = [ "xhci_pci" "thunderbolt" "nvme" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];
  boot.kernelParams = ["boot.shell_on_fail"];
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # Disk settings
  fileSystems."/" =
      { device = "/dev/disk/by-uuid/18ce8aa0-e6b3-4723-8d8c-7fbff27563bc";
        fsType = "ext4";
      };

  fileSystems."/boot" =
      { device = "/dev/disk/by-uuid/1610-2809";
        fsType = "vfat";
      };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/a305b082-df4c-4967-b107-55aea62569db"; }
    ];

  # power settings and services
  powerManagement.cpuFreqGovernor = lib.mkDefault "conservative";
  powerManagement.powertop.enable = true;
  powerManagement.enable = true;
  services.tlp.enable = true;

  time.timeZone = "America/Los_Angeles";

  # Network settings
  networking.hostName = "framework";
  networking.useDHCP = false;
  networking.interfaces.wlp170s0.useDHCP = true;
  networking.networkmanager.enable = true;
  networking.firewall.extraCommands = ''iptables -t raw -A OUTPUT -p udp -m udp --dport 137 -j CT --helper netbios-ns'';

  services.fprintd.enable = true;
  security.pam.services.login.fprintAuth = true;
  security.pam.services.xscreensaver.fprintAuth = true;

  # Note: Not setting up a display manager here because these are system level settings.
  # The display manager and desktop manager will live in the home-manager config at ../home-manager/framework-laptop.nix
  
  
  # Setup steam
  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.extraPackages32 = with pkgs.pkgsi686Linux; [ libva ];
  hardware.pulseaudio.support32Bit = true;
  programs.steam.enable = true;
}
